#ifndef SPHERE_H
#define SPHERE_H
#include <iostream>
#include <string>


class Sphere
{
public:

  Sphere (double p_rayon);
  double reqRayon() const;
  void asgRayon(double p_rayon);
  std::string reqSphereFormate() const;
  double reqVolume() const;
 
private:
  double m_rayon;

};



#endif /* SPHERE_H */

