
#include <sstream>
#include "Sphere.h"
static const double PI = 3.14;

Sphere::Sphere (double p_rayon):m_rayon(p_rayon){
	// on ne met rien ici puisqu'on a utilise la liste d'initialisation. La methode conseillee
}
double Sphere::reqRayon() const{
      return m_rayon;
}
void Sphere:asgRayon(double p_rayon){
	m_rayon = p_rayon;
}
string Sphere::reqSphereFormate() const{
       ostrinstream os;
       os << "Rayon: " << m_rayon << endl;
}

double Sphere::reqVolume() const{
	return 4.0/3.0 * PI * m_rayon*m_rayon*m_rayon;
}